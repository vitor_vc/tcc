module.exports = (sequelize, DataType) => {

    const Cerca = sequelize.define('Cerca', {

        IdCerca: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: true,
        },

        NomeCerca: {
            type: DataType.TEXT,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },

        pontoMapa: {
            type: DataType.TEXT,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },

    });
        

    return Cerca;
};
