const argon2 = require("argon2");
const { password } = require("../config/config");

module.exports = (sequelize, DataType) => {

    const users = sequelize.define('users', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            OnDelete: 'CASCADE',
            autoIncrement: true
        },
        
        email: {
            type: DataType.TEXT,
            primaryKey: false,
            OnDelete: 'CASCADE',
        },

        Nome: {
            type: DataType.TEXT,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },

        password: {
            type: DataType.TEXT,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },


    
    },
    {
        hooks: {
            
            async beforeCreate(user) {

                try {
                    const hash = await argon2.hash(user.password);
                    user.set('password', hash);
                } catch(e){
                    console.error(e);
                }
            }
        }
    });
        
    users.verifyPassword = async (hash,password) => {

        try{
            if(await argon2.verify(hash, password)) {

                return true;
            }
        } catch(e){
            console.error(e);
        }

        return false;
    }

    return users;
};
