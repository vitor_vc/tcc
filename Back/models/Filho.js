module.exports = (sequelize, DataType) => {

    const Filho = sequelize.define('Filho', {

        IdFilho: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: true,
        },

        Nome: {
            type: DataType.TEXT,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },

        Idade: {
            type: DataType.INTEGER,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false,
        },
    });
        

    return Filho;
};
