 const bodyParser = require("body-parser");
 const app = require('express')();
 const http = require('http').createServer(app);
 const io= require('socket.io')(http);


 http.listen(3000, function(){
    console.log('Express server listening on port ' + 3000);

    io.on('connection', (socket) => {
      console.log('a user connected');
  });

  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });
  

});


const config = require("./config/config");
const datasource = require("./config/datasource");

const indexRouter = require("./routes/index");
const UserRouter = require("./routes/users");
const FilhoRouter = require("./routes/filho");
const CercaRouter = require("./routes/cerca");
const authRouter = require("./routes/auth");

const authorization = require('./auth');



 

app.config = config;
app.datasource = datasource(app);

 app.use(bodyParser.json({
     limit: '5mb'
 }));

 const auth = authorization(app);
 app.use(auth.initialize());
 app.auth = auth;

 authRouter(app);
 indexRouter(app);
 UserRouter(app);
 FilhoRouter(app);
 CercaRouter(app);
 module.exports = app;
 