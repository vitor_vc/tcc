const CercaController = require('../controllers/cerca');

module.exports = (app) => {

    const cercaController = new CercaController(app.datasource.models.Cerca);

    app.route('/cerca')
        .get((req, res) => {

            cercaController
                .getAll()
                .then(data => {
                    res.json(data);
                })

                .catch(error => {
                    console.error(error);
                    res.status(400);
                });
        })
        .post((req, res) => {
            cercaController
                .create(req.body)
                .then(rs => {
                    res.json(rs);
                    res.status(201);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        });

    app.route('/cerca/:IdCerca')
        .get((req, res) => {

            cercaController
                .getById(req.params)
                .then(rs => {
                    res.json(rs);
                })
                .catch(error => {
                    console.error(error);
                    res.status(400);
                });
        })
        .put((req, res) => {

            cercaController
                .update(req.body, req.params)
                .then(rs => {
                    res.json(rs);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        })
        .delete((req, res) => {

            cercaController
                .delete(req.params)
                .then(rs => {
                    res.json(rs);
                    res.status(204);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        });
    
    };

