const FilhoController = require('../controllers/filho');

module.exports = (app) => {

    const filhoController = new FilhoController(app.datasource.models.Filho);

    app.route('/filho')
        .get((req, res) => {

            filhoController
                .getAll()
                .then(data => {
                    res.json(data);
                })

                .catch(error => {
                    console.error(error);
                    res.status(400);
                });
        })
        .post((req, res) => {
            filhoController
                .create(req.body)
                .then(rs => {
                    res.json(rs);
                    res.status(201);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        });

    app.route('/filho/:IdFilho')
        .get((req, res) => {

            filhoController
                .getById(req.params)
                .then(rs => {
                    res.json(rs);
                })
                .catch(error => {
                    console.error(error);
                    res.status(400);
                });
        })
        .put((req, res) => {

            filhoController
                .update(req.body, req.params)
                .then(rs => {
                    res.json(rs);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        })
        .delete((req, res) => {

            filhoController
                .delete(req.params)
                .then(rs => {
                    res.json(rs);
                    res.status(204);
                })
                .catch(error => {
                    console.error(error);
                    res.status(422);
                });
        });
    
    };

