const HttpStatus = require("http-status");

class CercaController {

    constructor(modelCerca) {
        this.Cerca = modelCerca;
    }

    getAll() {
        return this.Cerca
            .findAll({})
            .then(rs => rs)
            .catch(e => e);
    }

    getById(params) {
        return this.Cerca
            .findOne({ where: params })
            .then(rs => rs)
            .catch(e => e);
    }

    create(data) {
        return this.Cerca
            .create(data)
            .then(rs => rs)
            .catch(e => e);
    }

    update(data, params) {
        return this.Cerca
            .update({
                NomeCerca: data.NomeCerca,
                pontosMapa: data.pontosMapa,
            }, { where: params })
            .then(rs => rs)
            .catch(e => e);
    }

    delete(params) {
        return this.Cerca
            .destroy({ where: params })
            .then(rs => rs)
            .catch(e => e);
    }};

    module.exports = CercaController;
