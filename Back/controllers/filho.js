const HttpStatus = require("http-status");

class FilhoController {

    constructor(modelFilho) {
        this.Filho = modelFilho;
    }

    getAll() {
        return this.Filho
            .findAll({})
            .then(rs => rs)
            .catch(e => e);
    }

    getById(params) {
        return this.Filho
            .findOne({ where: params })
            .then(rs => rs)
            .catch(e => e);
    }

    create(data) {
        return this.Filho
            .create(data)
            .then(rs => rs)
            .catch(e => e);
    }

    update(data, params) {
        return this.Filho
            .update({
                Nome: data.Nome,
                Idade: data.Idade,
            }, { where: params })
            .then(rs => rs)
            .catch(e => e);
    }

    delete(params) {
        return this.Filho
            .destroy({ where: params })
            .then(rs => rs)
            .catch(e => e);
    }};

    module.exports = FilhoController;
