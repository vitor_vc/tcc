const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

var database = null;
var modelsByName = [];

const loadModels = (sequelize) => {
    const dir = path.join(__dirname, '../models');
    const models = [];

    fs.readdirSync(dir).forEach(file => {
        const modelPath = path.join(dir, file);
        const model =sequelize.import(modelPath);
        models[model.name] = model;
   

        modelsByName.push(models[model.name]);

    });

    return models;
};

module.exports = (app) => {
    if(!database){
        const config = app.config;
        const sequelize = new Sequelize(
            config.database,
            config.username,
            config.password,
            config.params
        );

        database = {
            sequelize,
            Sequelize,
            models: {}
        };

        database.models = loadModels(sequelize);
        console.log(modelsByName[0]);
        console.log(modelsByName[1]);
        console.log(modelsByName[2]);


        modelsByName[2].hasMany(modelsByName[1]);// User  tem varios Filhos
        modelsByName[1].belongsTo(modelsByName[2]);// Filhos pertence a User

        modelsByName[1].hasMany(modelsByName[0]);// Filhos  tem varias Cercas
        modelsByName[0].belongsTo(modelsByName[1]);// Cerca pertence a Filhos


        sequelize.sync().done(() => database);

    }

    return database;

};
