module.exports = {
    database: "test",
    username: "postgres",
    password: "v1i2t3o4r5",
    params: {
        dialect: "postgres",
        define:{
            underscored: false
        }
    },
    jwt: {
        secret: 't0p-S3cr3t',
        session: {session:false}
    }
};