import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import { DrawerItem} from '@react-navigation/drawer';

export function CustomDrawer(props){
    return(
<View style={styles.container}>
            <View style={styles.userArea}>
                <Image 
                source={require('../../assets/user.png')}
                style={styles.user}
                />
                <Text style={styles.nome} >Usuario </Text>
                <Text style={styles.email} >email@gmail.com </Text>

                <DrawerItem 
                label="Home"
                onPress={()=>{props.navigation.navigate('Home')}}    
                /> 

            </View>

        </View>

    );

}

const styles= StyleSheet.create({
    container:{
    flex:1,

    },
    userArea:{
        marginTop: 25,
        marginLeft: 10,
        marginBottom: 10,

    },
    user:{
        width: 55,
        height:55,
    },
    nome:{

        marginTop: 5,
        fontSize:18,
        fontWeight:'bold'
    },
    email:{
        fontSize: 15,
    },
    home:{
        fontSize: 20,
        fontWeight:'bold'
    }
    
});
