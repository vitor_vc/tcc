import React from "react";
import {
  StyleSheet,
  View,
  Platform,
  Dimensions,
  SafeAreaView,
  PermissionsAndroid,
  KeyboardAvoidingView,
  Text,
  Button,
} from "react-native";
import MapView, { Marker, AnimatedRegion, animateMarkerToCoordinate } from "react-native-maps";
import PubNub from "pubnub"    


const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;
const LATITUDE = -2.41000;
const LONGITUDE = -54.7000;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      coordinate: new AnimatedRegion({
        latitude: -2.41000,
        longitude: -54.7000,
        latitudeDelta: 0,
        longitudeDelta: 0
      })
    };
    this.pubnub = new PubNub({
      publishKey : "pub-c-db340f96-ec21-4bca-a870-c95ec7f64a91",
      subscribeKey : "sub-c-681bf69a-9621-11eb-9adf-f2e9c1644994",
    });
  }
  componentDidMount() {
   // this.callLocation()
    this.watchLocation()
  }


  componentDidUpdate(prevProps, prevState) {
    if (this.props.latitude !== prevState.latitude) {
      this.pubnub.publish({
        message: {
          latitude: this.state.latitude,
          longitude: this.state.longitude
        },
        channel: "location"
      });
      
    }
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
 }

 watchLocation = () => {
  navigator.geolocation.watchPosition(
    position => { console.log(position);
      const { latitude, longitude } = position.coords;


      this.setState({
        latitude,
        longitude,
      })

    },
    error => console.log(error),
    {
      enableHighAccuracy: true,
      timeout: 50,
      maximumAge: 0,
      distanceFilter: 1
    }
  );
};

getMapRegion = () => ({
  latitude: this.state.latitude,
  longitude: this.state.longitude,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA
});

  
  callLocation = () => {
      const requestPermission = async () => {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "Permissão de Acesso à Localização",
            message: "Este aplicativo precisa acessar sua localização.",
            buttonNegative: "Cancelar",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.watchLocation();
          
          
        } else {
          alert('Permissão de Localização negada');
        }
        
      };
      requestPermission();
    
  }
  

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <MapView style={styles.map} showUserLocation followUserLocation loadingEnabled region={this.getMapRegion()}>
      
          <Marker coordinate={this.getMapRegion()} />
          </MapView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
});