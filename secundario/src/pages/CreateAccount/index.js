import React, {useState} from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import api from '../../services/api'

export default function CriarConta() {
  const navigation = useNavigation();
  const [Nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

   function handleSingUp(){
       api.post('/users',{email, Nome, password});
   }

  return (
    <KeyboardAvoidingView style={styles.background}>
      <View>
        <Text style={styles.Texto}>
          Cadastro
        </Text>
      </View>

      <View style={styles.titulo} >
        <Text style={styles.Texto2} >Nome</Text>
      </View>

      <View style={styles.container} >
      <TextInput
      style={styles.input}
      autoCorrect={false}
      onChangeText={setNome}
      />
      </View>

      <View style={styles.titulo} >
        <Text style={styles.Texto2} >Email</Text>
      </View>

      <View style={styles.container} >
      <TextInput
      style={styles.input}
      autoCorrect={false}
      onChangeText={setEmail}
      />
      </View>


      <View style={styles.titulo} >
        <Text style={styles.Texto2} >Senha</Text>
      </View>

      <View style={styles.container} >
      <TextInput
      style={styles.input}
      autoCorrect={false}
      onChangeText={setPassword}
      />
      </View>
     <TouchableOpacity style={styles.btnSubmit}  onPressIn={handleSingUp} onPressOut={()=> navigation.navigate('SignIn')} >
       <Text style={styles.SubmitText}>Salvar</Text>
     </TouchableOpacity>

    </KeyboardAvoidingView>


  );
}

//
const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#404E7C',
    alignItems: 'center',
    justifyContent: 'center',   
    marginTop:24   
  },
  container:{
    flexDirection:'column',
 //   alignItems: 'center',
    justifyContent: 'center',    
    width:'90%',
    paddingBottom: 5
  },
  Texto:{
    alignItems: 'center',
    justifyContent: 'center',  
    fontSize: 40,
    marginBottom:40,
  },
  Texto2:{
   //alignItems: 'center',
    //justifyContent: 'center',  
    fontSize: 22,
    marginBottom:5,
  },
  input:{
    backgroundColor:'#FFF',
    width:'100%',
    marginBottom:15,
    color: '#222',
    fontSize: 17,
    borderRadius: 7,
    padding:10,
  },
  btnSubmit:{
    backgroundColor:'#FFF',
    width:'90%',
    height:45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7,
  },

  titulo:{   
    width:'90%',
  },

});
