import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '../pages/SignIn';
import CreateAccount from '../pages/CreateAccount'

const AuthStack = createStackNavigator();
const hideHeader = {headerShown: false};

const AuthRoutes = () => (
    <AuthStack.Navigator >
        <AuthStack.Screen name="SignIn" component={SignIn} options={hideHeader} />
        <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={hideHeader} />
    </AuthStack.Navigator>
)

export default AuthRoutes;