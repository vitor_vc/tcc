import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {CustomDrawer} from '../components/CustomDrawer'

import Dashboard from '../pages/Dashboard';
import Fance from '../pages/Fance'
import Monitoring from '../pages/Monitoring';

const AppDrawer = createDrawerNavigator();

const AppRoutes = () => (
    <AppDrawer.Navigator drawerContent={props => <CustomDrawer {...props}/>} >
        <AppDrawer.Screen name="Fance" component={Fance} />
        <AppDrawer.Screen name="Monitoring" component={Monitoring} />
        <AppDrawer.Screen name="Dashboard" component={Dashboard} />


    </AppDrawer.Navigator>
)

export default AppRoutes;