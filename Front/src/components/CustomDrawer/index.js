import React, { useContext, useState } from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import { Avatar, Title, Caption, Paragraph, Drawer,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons'; 
import AuthContext from '../../contexts/auth';



export function CustomDrawer(props){
    const { signed, user, signOut } = useContext(AuthContext);

    function handleSignout(){
        signOut();
    }

    return(
        <View style={{flex:1, backgroundColor: '#404E7C'}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
        

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem style={styles.Section}
                            icon={() => (
                            <MaterialIcons name="fence" size={24} color="black" />
                            )}
                            label="Cercas"
                            onPress={()=>{props.navigation.navigate('Fance')}}  
                        />

                        
                        <DrawerItem style={styles.Section}
                            icon={() => (
                                <MaterialCommunityIcons name="google-maps" size={24} color="black" />
                            )}
                            label="Monitorar"
                            onPress={()=>{props.navigation.navigate('Monitoring')}} 
                        />
                        
                    </Drawer.Section>

                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.Section}>
                <DrawerItem 
                    icon={() => (
                    <MaterialIcons name="exit-to-app" size={24} color="black" />
                    )}
                    label="Sair"
                    onPress={() => {handleSignout()}}
                />
            </Drawer.Section>
        </View>
    );

}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
    },
    Section:{
        marginHorizontal: 5,
        backgroundColor: "#FFF",
        borderRadius: 3,    
    },

    bottomDrawerSection: {
        marginBottom: 15,
        borderTopWidth: 1,
        backgroundColor: '#FFFF'
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });

