import React, { useContext, useState } from 'react';
import { View, Text, Image, StyleSheet, KeyboardAvoidingView, TouchableOpacity , TextInput, Button} from 'react-native';
import AuthContext from '../../contexts/auth';
import {useNavigation} from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';

export default function SignIn() {
  const navigation = useNavigation();
    const { signed, signIn, error } = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    function handleSignIn(){
        signIn(email,password);

    }

    return (
        <KeyboardAvoidingView style={styles.background}>
           <StatusBar style="dark" backgroundColor="#404E7C"/>
          <View style={styles.containerLogo} >
          <Image 
          source={require('../../assets/logo.png')}
          />
          
          </View>
    
          <View style={styles.container}>
          <TextInput
          style={styles.input}
          placeholder="Email"
          keyboardType="email-address"
          onChangeText={setEmail}
          autoCorrect={false}
       
          />
          <TextInput
          secureTextEntry={true}
           style={styles.input}
          placeholder="Senha"
          onChangeText={setPassword}
          autoCorrect={false}
          />
        <TouchableOpacity style={styles.btnSubmit} onPress={handleSignIn} >
           <Text style={styles.SubmitText}>Acessar</Text>
         </TouchableOpacity>
    
         <TouchableOpacity style={styles.btnSubmitRegister} onPress={()=> navigation.navigate('CreateAccount')} >
           <Text style={styles.SubmitText}>Criar conta gratuita</Text>
         </TouchableOpacity>
    
         </View>
    
        
        </KeyboardAvoidingView>
      );
}

const styles = StyleSheet.create({
    background: {
      flex: 1,
      backgroundColor: '#404E7C',
      alignItems: 'center',
      justifyContent: 'center',   
    },
    containerLogo:{
      flex:1,
      justifyContent:'center',
    },
    container:{
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',    
      width:'90%',
      paddingBottom: 50
    },
    input:{
      backgroundColor:'#FFF',
      width:'90%',
      marginBottom:15,
      color: '#222',
      fontSize: 17,
      borderRadius: 7,
      padding:10,
    },
    btnSubmit:{
      backgroundColor:'#FFF',
      width:'90%',
      height:45,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 7,
    },
    SubmitText:{
      color:'#000',
      fontSize: 18,
  
    },
    btnSubmitRegister:{
      marginTop: 10,
  
    }
  });