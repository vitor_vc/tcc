import React from 'react';
import { View, StyleSheet, Dimensions, SafeAreaView } from 'react-native';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import PubNubReact from "pubnub-react"  

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = -2.41000;
const LONGITUDE = -54.7000;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.0001;
class Tracker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      coordinate: new AnimatedRegion({
        latitude: -2.41000,
        longitude: -54.7000,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }),
    };

    // Replace "X" with your PubNub Keys
    this.pubnub = new PubNubReact({
      publishKey : "pub-c-db340f96-ec21-4bca-a870-c95ec7f64a91",
      subscribeKey : "sub-c-681bf69a-9621-11eb-9adf-f2e9c1644994",
      subscribeRequestTimeout: 60000,
      presenceTimeout: 122,
    });
    this.pubnub.init(this);
  }

  // code to receive messages sent in a channel
  componentDidMount() {
    
    this.subscribeToPubNub();
  }


  subscribeToPubNub = () => {
    this.pubnub.subscribe({
      channels: ['location'],
      withPresence: true,
    });
    this.pubnub.getMessage('location', msg => { 
      const { latitude, longitude } = msg.message;
      
      this.setState({
        latitude,
       longitude,
     });
    });
  };

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });


  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <MapView style={styles.map} showUserLocation followUserLocation loadingEnabled region={this.getMapRegion()}>
      
          <Marker coordinate={this.getMapRegion()} />
          </MapView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Tracker;