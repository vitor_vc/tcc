import React, { useContext } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import MapView, { Polygon,LatLng } from 'react-native-maps';
import { AntDesign } from '@expo/vector-icons'; 
import {useNavigation} from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import api from '../../services/api';
import AuthContext from '../../contexts/auth';
import { useState } from 'react';


export default function Dashboard() {
    const navigation = useNavigation();
    const { signed, user, signOut } = useContext(AuthContext);
    const [pontoMapa, setDrawnPolygon] = useState([{
      latitude: -2.435908,
      longitude: -54.720120,
    }]);
    const [count, setCount] = useState(0);
    const [loading, setLoading] = useState(false);
    const NomeCerca = 'teste';


    function postCerca(){
      api.post('/cerca',{NomeCerca,pontoMapa});
  }

    function addPolygon(coordinate) {
      if(count === 0) {
        setDrawnPolygon([coordinate]);
      } else {
        setDrawnPolygon(pontoMapa => [...pontoMapa, coordinate]);
      }

      setLoading(false);
      setCount(count+1);
    }

    function clearPolygon() {
      setDrawnPolygon([{
        latitude: -2.435908,
        longitude: -54.720120,
      }]);

      setCount(0);
    }

    return (
        <View style={styles.container}>
          <StatusBar style="dark" backgroundColor="#404E7C"/>
          
           <MapView 
                style={styles.map}
                onPress={({nativeEvent}) => {
                  if(nativeEvent.coordinate) {
                    const {coordinate, position} = nativeEvent;
                    setLoading(true);
                    addPolygon(coordinate);
                  }
                }}
                initialRegion={{
                  latitude: -2.435908,
                  longitude: -54.720120,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }} >
                  {!loading && (
                    <Polygon 
                      coordinates={pontoMapa} 
                      strokeWidth={2} 
                      strokeColor="rgb(255, 99, 71)" 
                      fillColor="rgb(255, 99, 71)" 
                    />
                  )}
            </MapView> 

             <TouchableOpacity onPress={() => {
               clearPolygon();
             }} style={styles.header2}>
               <View style={styles.inputArea2}>
               <AntDesign name="enviroment" size={24} color="black" />
                 <Text style = {styles.input}>
                 Apagar Cerca
                   </Text> 
               </View>
     
             </TouchableOpacity>
     
             <View style={styles.header3}>
             <TouchableOpacity onPressIn={postCerca} onPressOut={()=> navigation.navigate('Fance')}>
               <View  style={styles.save}>
                 <Text style = {styles.input2}>
                 Save
                   </Text> 
               </View>
               </TouchableOpacity>
     
               <TouchableOpacity onPress={()=> navigation.navigate('Fance')}>
               <View style={styles.save}>
                 <Text style = {styles.input2}>
                 Cancel
                   </Text> 
               </View>
               </TouchableOpacity>
             </View>
     
        </View>
     
     
       );
}

const styles = StyleSheet.create({
    header:{
      paddingHorizontal: 1,
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      marginVertical: 10,
    },
    
    header2:{
      paddingHorizontal: 1,
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      marginVertical: 1,
    },
  
  
    container:{
        flex:1,
        backgroundColor:'#404E7C',
        justifyContent: 'flex-start',
      },
  
      map: {
        width:"100%",
        height: "80%",
      },
  
      input:{
        fontSize: 13,
        paddingHorizontal:10,
        width: '90%',
      },
  
      inputArea2:{
        paddingVertical: 5,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        width: "98%",
        marginVertical: 10,
        backgroundColor: '#FFF',
        height: 37,
        borderRadius:1,
      },
  
      header3:{
        paddingHorizontal: 1,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-around',
        width:'100%',
        marginVertical: 10,
      },
  
      save:{
  
        alignItems: 'center',
        backgroundColor:'#FFF',
        paddingHorizontal: 63,
        height: 50,
        borderRadius:1,
  
      },
  
      input2:{
        fontSize: 20,
        alignItems: 'center',
        marginVertical: 10,
          },
  
  });