import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export default function Fance() {

    const navigation = useNavigation();

    return (
      
        <View style={styles.background}>
             <StatusBar style="dark" backgroundColor="#404E7C"/>
            <TouchableOpacity style={styles.Cercacontainer} onPress={()=> navigation.navigate('Dashboard')}>
               <Text style={styles.titulo} >Cerca 1</Text>
               <Text style={styles.descricao} >Descrição da Cerca</Text>
            </TouchableOpacity>
   
            <TouchableOpacity style={styles.Cercacontainer} onPress={()=> navigation.navigate('Monitoring')} >
               <Text style={styles.titulo} >Cerca 2</Text>
               <Text style={styles.descricao} >Descrição da Cerca</Text>
            </TouchableOpacity>
   
            <TouchableOpacity style={styles.Cercacontainer} >
               <Text style={styles.titulo} >Cerca 3</Text>
               <Text style={styles.descricao} >Descrição da Cerca</Text>
            </TouchableOpacity>
   
            <TouchableOpacity style={styles.Cercacontainer} >
               <Text style={styles.titulo} >Cerca 4</Text>
               <Text style={styles.descricao} >Descrição da Cerca</Text>
            </TouchableOpacity>
        </View>
   
     );

}

const styles = StyleSheet.create({
    background: {
      flex: 1,
      backgroundColor: '#404E7C',
      marginTop:24  
    },
   Cercacontainer:{
      marginHorizontal: 10,
      marginVertical:7,
      padding: 20,
      backgroundColor: "#FFF",
      borderRadius: 3,    
    },
    titulo:{
      alignItems: 'center',
      justifyContent: 'center',  
      fontSize: 20,
      marginBottom:3,
    },
  });
  