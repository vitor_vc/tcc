O Projeto está dividido em duas Pastas, Back que contém a API com o servidor e Front contendo a aplicação móvel

///////////////////////////////////////BACKEND/////////////////////////////////////////////

Instalar o banco de dados PostgreSQL -> (https://www.postgresql.org/download/)

Criar um Banco com o nome "test"
Setar o Login e Senha do PostgreSQL no arquivo "config.js" localizado em "tcc\Back\config\config.js"
Exemplo:

database: "test",
    username: "postgres",
    password: "v1i2t3o4r5",

Instalar o NodeJs -> (https://nodejs.org/en/download/)

Instalar os seguintes Pacotes:

"argon2"      -> npm i argon2 --save
"bcrypt"      -> npm i bcrypt --save
"body-parser" -> npm i body-parser --save
"cors"        -> npm i cors
"express"     -> npm i express --save
"http-status" -> npm i http-status --save
"pg-hstore"   -> npm i pg pg-hstore --save
"sequelize"   -> npm i sequelize --save
"jsonwebtoken"-> npm i jsonwebtoken passport passport-jwt --save

Após os seguintes passos executar o comando "npm start" no terminal para iniciar o servidor


/////////////////////////////////////FRONTEND///////////////////////////////////////////	Set-ExecutionPolicy unrestricted

Instalar o Expo através do comando npm install --global expo-cli

Criar um projeto utilizando o comando "expo init nome_app"

Copiar o código para o projeto

Instalar os seguintes pacotes:
"axios"			  -> expo install axios
"react-navigation"        -> expo install @react-navigation/native
"react-native-maps"	  -> expo install react-native-maps
"gesture handler"         -> expo install react-native-gesture-handler
"reanimated"              -> expo install react-native-reanimated
"screens"                 -> expo install react-native-screens
"safe-area-context"       -> expo install react-native-safe-area-context 
"masked-view"             -> expo install @react-native-community/masked-view
"react-navigation/drawer" -> expo install @react-navigation/drawer
"react-navigationm/stack" -> expo install @react-navigation/stack
"async storage"           -> expo install @react-native-community/async-storage

Para a integração do back com o front é necessário colocar o ip da sua máquina no arquivo index.js localizado em tcc\Front\src\services\api\index.js

com o IP em mãos basta troca-lo
Exemplo:

const api = axios.create({
  baseURL: 'http://10.0.1.197:3000',
});

após o IP é necessário ter :3000 que se trata da porta utilizada

Iniciar o projeto utilizando o comando "expo start"

PARA UTILIZAR O PROJETO É NECESSÁRIO TER UM EMULADOR ANDROID INSTALADO OU UTILIZAR O APLICATIVO EXPO GO 


  


